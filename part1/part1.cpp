// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
using namespace std;

char answer{ 0 };

bool accept()
{
    cout << "Do you wish to proceed (y or n)?\n";
    cin >> answer;

    if (answer == 'y') return true;
    return false;
}

int main()
{
    int i{ 3 };
    constexpr int b = 2343 * 2;
    vector<int> stuff{ 1,2,3,4,5 };
    cout << "Hello World!\n";
    accept();
}